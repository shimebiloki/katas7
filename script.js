function newForEach(array, callback) {
    for (let i = 0; i < array.length; i++) {
        const valor = array[i]
        callback(valor, i, array)
    }
}

 const letters = ['a', 'b', 'c', 'd', 'e']
newForEach(letters, function (letter, index, array) {
    console.log(letter, index, array)
})

//map
function newMap(arr, callback) {
    const newArr5 = []
    for(let i = 0; i < arr.length; i++) {
        newArr5.push(callback(arr[i], i, arr))
    }
    return newArr5
}
function exemplo (num) {
    return num * 2
}
let arr5 = [1, 2, 3, 4, 5, 6, 7, 8]

console.log(newMap(arr5, exemplo))

//some
function newSome(arr, valor, callback) {
    let resultado = false
    for (let i = 0; i < arr.length; i++) {
        if(callback(arr[i], i, arr) === valor) {
            resultado = true
        }
    }
    return resultado
}
function exemplo2 (num) {
    return num
}

console.log(newSome([4, 5, 4, 4, 3, 2, 5], 7, exemplo2))
 
//find
function newFind(arr, callback) {
    const arrayNovo = []
    for(let i = 0; i < arr.length; i++) {
        if(callback(arr[i], i, arr)) {
            arrayNovo.push(callback(arr[i], i, arr))
        }
    }
    return arrayNovo[0]
}
 function exemplo3(num) {
    if(num > 35) {
        return num
    }
}

console.log(newFind([20, 30, 40, 50, 60], exemplo3)) 


    //findIndex
    function newFindIndex(arr, callback) {
        for(let i = 0; i < arr.length; i++) {
            if(callback(arr[i], i, arr)) {
                (callback(arr[i], i, arr))
                return i
            }
        }
    }
     function exemplo6(num) {
        if(num > 35) {
            return num
        }
    }
    
    console.log(newFindIndex([10, 20, 30, 40, 50, 60], exemplo6))
        
    //every
    function newEvery(arr, valor, callback) {
        let resultado = true
        for (let i = 0; i < arr.length; i++) {
            if(callback(arr[i], i, arr) !== valor) {
                resultado = false
            }
        }
        return resultado
    }
    function exemplo4 (num) {
        return num
    }

    console.log(newEvery([5, 5, 5, 5, 5, 5, 5], 5, exemplo4))

     //filter
    function newFilter(arr, callback) {
        const newArray = []
        for(let i = 0; i < arr.length; i++) {
            if((callback(arr[i], i, arr))) {
                newArray.push(callback(arr[i], i, arr))
            }
        }
        return newArray
    }
    function exemplo5(num) {
        if(num > 35) {
            return num
        }
    }
    console.log(newFilter([20, 30, 40, 50, 60], exemplo5))